/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-26
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.grid {
import ca.openbracket.unit.Unit;

import flash.geom.Point;

public class GridHighlighter {
    public static function highlight(grid:Grid, unit:Unit):void{
        switch(unit.unitType){
            case "fighter":
                grid.setCellHighlight("fighter", new Point(unit.position.x+1,unit.position.y));
                grid.setCellHighlight("fighter", new Point(unit.position.x + 2, unit.position.y));
                grid.setCellHighlight("fighter", new Point(unit.position.x - 1, unit.position.y));
                grid.setCellHighlight("fighter", new Point(unit.position.x - 2, unit.position.y));
                grid.setCellHighlight("fighter", new Point(unit.position.x, unit.position.y - 1));
                grid.setCellHighlight("fighter", new Point(unit.position.x, unit.position.y - 2));
                grid.setCellHighlight("fighter", new Point(unit.position.x, unit.position.y + 1));
                grid.setCellHighlight("fighter", new Point(unit.position.x, unit.position.y + 2));
                break;
            case "scanner":
                grid.setCellHighlight("scanner", new Point(unit.position.x + 1, unit.position.y));
                grid.setCellHighlight("scanner", new Point(unit.position.x - 1, unit.position.y));
                grid.setCellHighlight("scanner", new Point(unit.position.x, unit.position.y - 1));
                grid.setCellHighlight("scanner", new Point(unit.position.x, unit.position.y + 1));
                break;
            case "jammer":
                grid.setCellHighlight("jammer", new Point(unit.position.x + 1, unit.position.y));
                grid.setCellHighlight("jammer", new Point(unit.position.x - 1, unit.position.y));
                grid.setCellHighlight("jammer", new Point(unit.position.x, unit.position.y - 1));
                grid.setCellHighlight("jammer", new Point(unit.position.x, unit.position.y + 1));
                break;
        }
    }
}
}
