/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-25
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.grid {
import starling.events.Event;

public class GridEvent extends Event{
    public static const UNIT_SELECTED:String = "UNIT_SELECTED";
    public static const UNIT_DESELECTED:String = "UNIT_DESELECTED"

    public function GridEvent(type:String,bubbles:Boolean=false, data:Object=null):void {
        super(type, bubbles ,data );
    }
}
}
