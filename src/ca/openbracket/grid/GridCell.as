/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.grid {
import ca.openbracket.unit.Unit;

import flash.geom.Point;

public class GridCell extends Object{
    public var pos:Point;
    public var unit:Unit;

    public function GridCell(position:Point):void{
        pos = position;
    }
}
}
