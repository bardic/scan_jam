/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-25
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.grid {
import ca.openbracket.Atlas;
import ca.openbracket.attack.Attack;
import ca.openbracket.cell.CellHighlight;
import ca.openbracket.unit.Unit;

import citrus.objects.CitrusSprite;

import flash.geom.Point;

import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class Grid  extends Sprite{
    private var gridSprite:CitrusSprite;
    private var gridArr:Array = [];
    private var selectedUnit:Unit;
    private var activeHighlights:Array = [];
    private var selectedCellHighlight:CellHighlight;
    public function Grid() {
        gridSprite = new CitrusSprite("gridSprite",{view:new Image(Atlas.getSprite("grid"))})
        gridSprite.view.addEventListener(TouchEvent.TOUCH, onCellClicked);
        addChild(gridSprite.view as DisplayObject);
        selectedCellHighlight = new CellHighlight(CellHighlight.SELECTED_CELL);
        createGrid();
    }

    private function createGrid():void {
        var gridCell:GridCell;
        var cols:int = 10;
        var rows:int = 10;
        for(var i:int = 0; i < rows; i++){
            gridArr.push([]);
            for(var j:int = 0; j < cols; j++){
                gridCell = new GridCell(new Point(j, i));
                gridArr[i].push(gridCell);
            }
        }
    }

    private function onCellClicked(ev:TouchEvent):void {
        var touch:Touch = ev.getTouch(ev.target as DisplayObject);
        if(touch){
            switch (touch.phase){
                case TouchPhase.BEGAN:
                    var pos:Point = touch.getLocation(ev.target as DisplayObject);
                    var gridX:int = Math.floor(pos.x/50);
                    var gridY:int = Math.floor(pos.y/50);

                    if(selectedUnit){
                        if(!selectedUnit.isTurnComplete){
                            if(checkIfLegalMove(selectedUnit, new Point(gridX,gridY))){
                                (gridArr[selectedUnit.position.x][selectedUnit.position.y] as GridCell).unit = null;
                                selectedUnit.position = new Point(gridX,gridY);
                                positionAndAddOnGrid(selectedUnit);
                                selectedUnit.isTurnComplete = true;
                            }else if(checkIfIsAttack(selectedUnit, new Point(gridX,gridY))){
                                selectedUnit.isTurnComplete = true;
                                Attack.PerformAttack(selectedUnit, (gridArr[gridX][gridY] as GridCell).unit);
                            }
                        }
                        clearCellHighlight();
                        selectedUnit = null;
                        dispatchEvent(new GridEvent(GridEvent.UNIT_DESELECTED,true));
                    }else{
                        if((gridArr[gridX][gridY] as GridCell).unit && !((gridArr[gridX][gridY] as GridCell).unit as Unit).isEnemy){
                            selectedUnit = (gridArr[gridX][gridY] as GridCell).unit;
                            if(!selectedUnit.isTurnComplete){
                                trace("Select a unit with turns");
                                setCellHighlight("fighter", new Point(gridX, gridY));
                                GridHighlighter.highlight(this, selectedUnit);
                                dispatchEvent(new GridEvent(GridEvent.UNIT_SELECTED, true, gridArr[gridX][gridY] as GridCell))
                            }
                        }
                    }
                    break;
                case TouchPhase.ENDED:
                    break;
                case TouchPhase.MOVED:
                    break;
            }
        }
    }

    private function checkIfIsAttack(selectedUnit:Unit, point:Point):Boolean {
        var isAttack:Boolean = false;
        if(((Math.abs(point.x - selectedUnit.position.x) <= 2 && point.y == selectedUnit.position.y) ||
                (Math.abs(point.y - selectedUnit.position.y) <= 2 && point.x == selectedUnit.position.x)) &&
                checkCellForUnits(point)){
            isAttack = true;
        }

        return isAttack;
    }

    private function checkIfLegalMove(selectedUnit:Unit, point:Point):Boolean {
        var isLegal:Boolean = false;
        switch(selectedUnit.unitType){
            case "fighter":
                trace("Check fighter move");
                if(((Math.abs(point.x - selectedUnit.position.x) <= 2 && point.y == selectedUnit.position.y) ||
                        (Math.abs(point.y - selectedUnit.position.y) <= 2 && point.x == selectedUnit.position.x)) &&
                        !checkCellForUnits(point)){
                    isLegal = true;
                }
                break;
            case "scanner":
                if(((Math.abs(point.x - selectedUnit.position.x) <= 1 && point.y == selectedUnit.position.y) ||
                        (Math.abs(point.y - selectedUnit.position.y) <= 1 && point.x == selectedUnit.position.x)) &&
                        !checkCellForUnits(point)){
                    isLegal = true;
                }
                break;
            case "jammer":
                if(((Math.abs(point.x - selectedUnit.position.x) <= 1 && point.y == selectedUnit.position.y) ||
                        (Math.abs(point.y - selectedUnit.position.y) <= 1 && point.x == selectedUnit.position.x)) &&
                        !checkCellForUnits(point)){
                    isLegal = true;
                }
                break;
        }
        return isLegal;
    }

    private function checkCellForUnits(p:Point):Boolean {
        var hasUnit:Boolean = false;
        if((gridArr[p.x][p.y] as GridCell).unit){
            hasUnit = true;
        }
        return hasUnit
    }

    public function positionAndAddOnGrid(unit:Unit):void {
        var gc:GridCell = gridArr[unit.position.x][unit.position.y] as GridCell;
        gc.unit = unit;
        (unit as CitrusSprite).view.x = unit.position.x * 50;
        (unit as CitrusSprite).view.y = unit.position.y * 50;
        ((unit as CitrusSprite).view as DisplayObject).touchable = false;
        addChild(unit.view as DisplayObject);
    }

    public function setCellHighlight(unitType:String, gridPoint:Point):void {
        if(gridPoint.x < 0 || gridPoint.y < 0 || gridPoint.x > 10 || gridPoint.y > 10){
            return;
        }
        var highlight:CellHighlight;
        if(unitType == "fighter"){
            var cell:GridCell = gridArr[gridPoint.x][gridPoint.y] as GridCell;
            if(cell.unit && cell.unit.isEnemy){
                highlight = new CellHighlight(CellHighlight.JAMMED_CELL);
            }else{
                highlight = new CellHighlight(CellHighlight.MOVE_RANGE_CELL);
            }
        }else{
           highlight = new CellHighlight(CellHighlight.MOVE_RANGE_CELL);
        }

        highlight.view.x = gridPoint.x * 50;
        highlight.view.y = gridPoint.y * 50;
        (highlight.view as DisplayObject).touchable = false;
        addChild(highlight.view as DisplayObject);
        activeHighlights.push(highlight);
    }

    private function clearCellHighlight():void{
        var activeHighlightsLen:int = activeHighlights.length;
        for(var i:int = 0;i< activeHighlightsLen;i++){
            removeChild(activeHighlights[i].view);
        }

        activeHighlights = [];
    }
}
}
