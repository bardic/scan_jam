/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.cell {
import ca.openbracket.Atlas;

import citrus.objects.CitrusSprite;

import starling.display.Image;

public class CellHighlight extends CitrusSprite{
    public static const JAMMED_CELL:String = "jammed";
    public static const SCANNED_CELL:String = "scanned";
    public static const MOVE_RANGE_CELL:String = "move_range";
    public static const SELECTED_CELL:String = "selected";
    public function CellHighlight(name:String, params:Object = null) {
        super(name, params);
        switch(name){
            case JAMMED_CELL:
                this.view = new Image(Atlas.getSprite("jammed_cell"));
                break;
            case SCANNED_CELL:
                break;
            case MOVE_RANGE_CELL:
                this.view = new Image(Atlas.getSprite("move_range_cell"));
                break;
            case SELECTED_CELL:
                this.view = new Image(Atlas.getSprite("select_cell"));
                break;
        }
    }


}
}
