/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-25
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.attack {
public class AttackOutcome {
    public static const UNIT_DAMAGED:String = "damaged";
    public static const UNIT_DESTROYED:String = "destroyed";
    private var _outcome:String;
    private var _data:Object;
    public function AttackOutcome(attackOutcome:String, attackData:Object) {
        _outcome = attackOutcome;
        _data = attackData;
    }

    public function get outcome():String {
        return _outcome;
    }

    public function get data():Object {
        return _data;
    }
}
}
