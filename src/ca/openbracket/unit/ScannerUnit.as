/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.unit {
import ca.openbracket.Atlas;

import flash.geom.Point;

import starling.display.Image;

public class ScannerUnit extends Unit{
    public function ScannerUnit(name:String, pos:Point,enemy:Boolean = false,params:Object=null) {
        super(name,pos,params);
        this.view = new Image(Atlas.getSprite("scanner"));
        unitType = "scanner";
        health = 1;
        movement = 1;
        isEnemy = enemy;
    }
}
}
