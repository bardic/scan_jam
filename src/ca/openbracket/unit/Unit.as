/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.unit {
import citrus.objects.CitrusSprite;

import flash.geom.Point;

public class Unit extends CitrusSprite implements IUnit{
    private var _unitType:String = "";
    private var _position:Point = new Point(0,0);
    private var _isEnemy:Boolean = false;
    private var _movement:int = 0;
    private var _health:int = 0;
    private var _attackValue:int = 0;
    private var _isTurnComplete:Boolean = false;
    private var _isJammed:Boolean = false;
    public var unitSignal:UnitSignal;
    public function Unit(name:String,pos:Point, params:Object) {
        super(name, params);
        unitSignal = new UnitSignal();
        position = pos;
    }

    public function get movement():int {
        return _movement;
    }

    public function set movement(i:int):void {
        _movement = i;
    }

    public function get health():int {
        return _health;
    }

    public function set health(i:int):void {
        _health = i;
    }

    public function set takeDamage(i:int):void{
        _health -= i;
        if(_health <= 0){
            unitSignal.dispatch(UnitSignal.UNIT_DESTROYED, this);
        }else{
            unitSignal.dispatch(UnitSignal.UNIT_DAMAGED, this);
        }
    }

    public function get unitType():String {
        return _unitType;
    }

    public function set unitType(s:String):void {
        _unitType = s;
    }

    public function get position():Point {
        return _position;
    }

    public function set position(p:Point):void {
        _position = p;
    }


    public function get isEnemy():Boolean {
        return _isEnemy;
    }

    public function set isEnemy(b:Boolean):void {
        _isEnemy = b;
    }

    public function get attackValue():int {
        return _attackValue;
    }

    public function set attackValue(i:int):void {
        _attackValue = i;
    }


    public function get isTurnComplete():Boolean {
        return _isTurnComplete;
    }

    public function set isTurnComplete(b:Boolean):void {
        _isTurnComplete = b;
        if(_isTurnComplete)
            unitSignal.dispatch(UnitSignal.UNIT_MOVED, this);
    }


    public function get isJammed():Boolean {
        return _isJammed;
    }

    public function set isJammed(b:Boolean):void {
        _isJammed = b;
    }
}
}
