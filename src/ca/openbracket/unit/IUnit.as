/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.unit {
import flash.geom.Point;

public interface IUnit {
    function get movement():int;
    function set movement(i:int):void;
    function get health():int;
    function set health(i:int):void;
    function set takeDamage(i:int):void;
    function get unitType():String;
    function set unitType(s:String):void;
    function get position():Point;
    function set position(p:Point):void;
    function get isEnemy():Boolean;
    function set isEnemy(b:Boolean):void;
    function get attackValue():int;
    function set attackValue(i:int):void;
    function get isTurnComplete():Boolean;
    function set isTurnComplete(b:Boolean):void;
    function get isJammed():Boolean;
    function set isJammed(b:Boolean):void;

}
}
