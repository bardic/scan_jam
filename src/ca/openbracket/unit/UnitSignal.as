/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-26
 * Time: 10:11 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.unit {
import org.osflash.signals.Signal;

public class UnitSignal extends Signal{
    public static const UNIT_DAMAGED:String = "damaged";
    public static const UNIT_DESTROYED:String = "destroyed";
    public static const UNIT_MOVED:String = "moved";
    public function UnitSignal() {
        super (String, Unit)
    }
}
}
