/**
 * User: thomasgillis
 * Date: 2013-01-24
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.unit {
import ca.openbracket.Atlas;

import flash.geom.Point;

import starling.display.Image;

public class FighterUnit extends Unit{
    public function FighterUnit(name:String, pos:Point, enemy:Boolean = false, params:Object=null) {
        super(name,pos, params);
        this.view = new Image(Atlas.getSprite("fighter"));
        unitType = "fighter";
        movement = 2;
        health = 2;
        isEnemy = enemy;
        attackValue = 1;
    }
}
}
