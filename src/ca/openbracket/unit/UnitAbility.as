/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-26
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.unit {
import ca.openbracket.grid.Grid;
import ca.openbracket.grid.GridCell;

public class UnitAbility extends Object{
    public function UnitAbility() {
    }

    public static function JamUnits(jammer:Unit, grid:Array):void{
        var unitsAroundArr:Array = [];
        unitsAroundArr.push((grid[jammer.position.x+1][jammer.position.y] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x+2][jammer.position.y] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x-1][jammer.position.y] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x-2][jammer.position.y] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x][jammer.position.y+1] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x][jammer.position.y+2] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x][jammer.position.y-1] as GridCell).unit);
        unitsAroundArr.push((grid[jammer.position.x][jammer.position.y-2] as GridCell).unit);

        var unit:Unit;
        for(var i:int; i < unitsAroundArr.length;i++){
            unit  = unitsAroundArr[i] as Unit;
            if(unit){
                unit.isJammed = true;
            }
        }
    }

    private function createUnitsAround(cell:GridCell):Unit{
        var unit:Unit = null;
        if(cell.unit)
            unit = cell.unit;

        return unit;

    }

    public static function ScanForUnits(scanner:Unit, grid:Grid):void{

    }

    public static function PerformAttack(attacker:Unit,defender:Unit):void{
        defender.takeDamage = attacker.attackValue;
    }
}
}
