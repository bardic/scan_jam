/**
 * User: thomasgillis
 * Date: 2013-01-23
 * Email: bardic.knowledge@gmail.com
 */
package ca.openbracket.states {
import ca.openbracket.Atlas;
import ca.openbracket.grid.Grid;
import ca.openbracket.grid.GridCell;
import ca.openbracket.grid.GridEvent;
import ca.openbracket.overview.Overview;
import ca.openbracket.service.PlayerioService;
import ca.openbracket.unit.FighterUnit;
import ca.openbracket.unit.JammerUnit;
import ca.openbracket.unit.ScannerUnit;
import ca.openbracket.unit.Unit;
import ca.openbracket.unit.UnitSignal;

import citrus.core.starling.StarlingState;

import flash.geom.Point;
public class GameState extends StarlingState{
    private var unitOverview:Overview;
    private var grid:Grid;
    private var unitArr:Array = [];
    private var playerioService:PlayerioService
    public function GameState(playerioService:PlayerioService) {
        this.playerioService = playerioService;
        super();
    }

    override public function initialize():void {
        super.initialize();
        Atlas.init();

        grid = new Grid();
        grid.addEventListener(GridEvent.UNIT_SELECTED, onUnitSelected);
        grid.addEventListener(GridEvent.UNIT_DESELECTED, onUnitDeselected);

        addChild(grid);
        unitOverview = new Overview();
        unitOverview.y = 500;
        addChild(unitOverview);

        addUnits();
        addEnemies();
    }

    private function onUnitDeselected(ev:GridEvent):void {
        unitOverview.clear();
    }

    private function onUnitSelected(ev:GridEvent):void {
        unitOverview.setSelectedUnit((ev.data as GridCell).unit);
    }

    private function addEnemies():void {
        var fighter:FighterUnit = new FighterUnit("fighter", new Point(7,5),true);
        grid.positionAndAddOnGrid(fighter);

        fighter = new FighterUnit("fighter", new Point(8,4),true);
        grid.positionAndAddOnGrid(fighter);

        fighter = new FighterUnit("fighter",  new Point(8,6),true);
        grid.positionAndAddOnGrid(fighter);

        var scanner:ScannerUnit = new ScannerUnit("scanner", new Point(9,5),true);
        grid.positionAndAddOnGrid(scanner);

        var jammer:JammerUnit = new JammerUnit("jammer",new Point(8,5),true);
        grid.positionAndAddOnGrid(jammer);
    }

    private function addUnits():void{
        var fighter:FighterUnit = new FighterUnit("fighter", new Point(2,5));
        grid.positionAndAddOnGrid(fighter);
        unitArr.push(fighter);

        fighter = new FighterUnit("fighter", new Point(1,4));
        grid.positionAndAddOnGrid(fighter);
        unitArr.push(fighter);

        fighter = new FighterUnit("fighter",  new Point(1,6));
        grid.positionAndAddOnGrid(fighter);
        unitArr.push(fighter);

        var scanner:ScannerUnit = new ScannerUnit("scanner", new Point(0,5));
        grid.positionAndAddOnGrid(scanner);
        unitArr.push(scanner);

        var jammer:JammerUnit = new JammerUnit("jammer",new Point(1,5));
        grid.positionAndAddOnGrid(jammer);
        unitArr.push(jammer);

        for(var i:int =0; i < unitArr.length; i++){
            (unitArr[i] as Unit).unitSignal.add(onUnitSignal)
        }
    }

    private function onUnitSignal(type:String, unit:Unit):void {
        trace("Unit Signal: " + type);
        switch (type){
            case UnitSignal.UNIT_DAMAGED:
                break;
            case UnitSignal.UNIT_DESTROYED:
                break;
            case UnitSignal.UNIT_MOVED:

                    if(checkIfEndsTurn()){
                        trace("End the players turn");
                    }
                break;
        }
    }

    private function checkIfEndsTurn():Boolean {
        var unit:Unit;
        var endTurn:Boolean = true;
        for(var i:int = 0; i < unitArr.length; i++){
            unit = unitArr[i];
            if(!unit.isTurnComplete){
                endTurn = false;
                trace("There is a unit that hasn't moved yet");
            }
        }

        return endTurn;
    }

    override public function destroy():void {
        super.destroy();
    }

    override public function update(timeDelta:Number):void {
        super.update(timeDelta);
    }
}
}
