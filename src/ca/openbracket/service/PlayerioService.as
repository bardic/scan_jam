/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-29
 * Time: 8:19 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.service {
import flash.display.Stage;

import playerio.Client;
import playerio.Connection;
import playerio.Message;
import playerio.PlayerIO;
import playerio.PlayerIOError;

public class PlayerioService extends Object{
    public function init(stage:Stage):void {
        PlayerIO.connect(
                stage,								//Referance to stage
                "testgame-tugswnxlb0qrmkjr91wzw",		//Game id (Get your own at playerio.com. 1: Create user, 2:Goto admin pannel, 3:Create game, 4: Copy game id inside the "")
                "public",							//Connection id, default is public
                "GuestUser",						//Username
                "",									//User auth. Can be left blank if authentication is disabled on connection
                null,								//Current PartnerPay partner.
                onConnect,						//Function executed on successful connect
                onError							//Function executed if we recive an error
        );
    }

    private function onConnect(client:Client):void{
        trace("Sucessfully connected to player.io");

        //Set developmentsever (Comment out to connect to your server online)
        client.multiplayer.developmentServer = "127.0.0.1:8184";

        //Create pr join the room test
        client.multiplayer.createJoinRoom(
                "fridgemagnets",					//Room id. If set to null a random roomid is used
                "FridgeMagnets",					//The game type started on the server
                true,								//Should the room be visible in the lobby?
                {},									//Room data. This data is returned to lobby list. Variabels can be modifed on the server
                {},									//User join data
                onJoin,							//Function executed on successful joining of the room
                onError							//Function executed if we got a join error
        );
    }

    private function onJoin(connection:Connection):void{
        trace("Sucessfully connected to the multiplayer server");
    }

    private function handleMessages(m:Message){
        trace("Recived the message", m)
    }

    private function handleDisconnect():void{
        trace("Disconnected from server")
    }

    private function onError(error:PlayerIOError):void{
        trace("Got", error);
    }
}
}
