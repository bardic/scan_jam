/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-25
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket.overview {
import ca.openbracket.unit.Unit;
import starling.display.Sprite;
import starling.text.TextField;

public class Overview extends Sprite{
    private var unitTypeTF:TextField;
    private var unitHealthTF:TextField;
    private var unitMovementTF:TextField;
    public function Overview() {

        unitTypeTF = new TextField(100,20,"");
        unitHealthTF = new TextField(100,20,"");
        unitMovementTF = new TextField(100,20,"");

        unitTypeTF.x = 0;
        unitTypeTF.y = 5;
        addChild(unitTypeTF)

        unitHealthTF.x = 100;
        unitHealthTF.y = 5;
        addChild(unitHealthTF)

        unitMovementTF.x = 200;
        unitMovementTF.y = 5;
        addChild(unitMovementTF)

    }

    public function setSelectedUnit(unit:Unit):void{
        unitTypeTF.text = "Unit:" + unit.unitType;
        unitHealthTF.text = "Health: " +unit.health.toString();
        unitMovementTF.text = "Range: " + unit.movement.toString();
    }

    public function clear():void {
        unitTypeTF.text = "";
        unitHealthTF.text = "";
        unitMovementTF.text = "";
    }
}
}
