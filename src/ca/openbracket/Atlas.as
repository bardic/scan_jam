/**
 * Created with IntelliJ IDEA.
 * User: tgillis
 * Date: 2013-01-26
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
package ca.openbracket {
import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class Atlas extends Object{
    [Embed(source="/atlas.xml", mimeType="application/octet-stream")]
    public static const AtlasXml:Class;

    [Embed(source="/atlas.png")]
    public static const AtlasTexture:Class;

    private static var atlas:TextureAtlas;

    public static function init() {
        var texture:Texture = Texture.fromBitmap(new AtlasTexture());
        var xml:XML = XML(new AtlasXml());
        atlas = new TextureAtlas(texture, xml);
    }

    public static function getSprite(name:String):Texture{
        var texture:Texture = atlas.getTexture(name);
        return texture;
    }
}
}
