package {

import ca.openbracket.service.PlayerioService;
import ca.openbracket.states.GameState;

import citrus.core.starling.StarlingCitrusEngine;

import flash.geom.Rectangle;

[SWF(backgroundColor="#ffffff", frameRate="60", width="500", height="550")]
public class ScanJam extends StarlingCitrusEngine {
    private var playerioService:PlayerioService;
    public function ScanJam() {
        setUpStarling(true);
        playerioService = new PlayerioService();
        playerioService.init(this.stage)
        state = new GameState(playerioService);

    }

    override public function setUpStarling(debugMode:Boolean = false, antiAliasing:uint = 1, viewport:Rectangle = null):void {
        super.setUpStarling(debugMode, antiAliasing, viewport);
    }
}
}
